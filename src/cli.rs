use clap::{Parser, Subcommand};

use crate::todo::Priority;

#[derive(Parser, Debug)]
#[command(version, about)]
pub struct Cli {
    #[command(subcommand)]
    pub action: Option<Action>,
}

#[derive(Subcommand, Debug)]
pub enum Action {
    /// Adds a new todo by name
    #[command(alias = "a")]
    Add {
        /// The name of the todo you want to add
        todo_name: String,
    },
    /// Removes a todo by name
    #[command(alias = "r")]
    Remove {
        /// The name of the todo you want to remove
        todo_name: String,
    },
    /// Shows the status of all todos
    #[command(alias = "s")]
    Status,
    /// Toggles the status of a todo between done and todo
    #[command(alias = "t")]
    Toggle {
        /// The name of the todo you want to toggle
        todo_name: String,
    },
    /// Modifies the priority of a todo
    #[command(alias = "p")]
    Priority {
        /// The name of the todo you want to modify
        todo_name: String,
        /// The priority you want to set
        priority: Priority,
    },
    /// Removes all tasks which are marked as done
    #[command(alias = "c")]
    Clean,
}
