use chrono::{DateTime, Local};
use clap::ValueEnum;
use colored::Colorize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Todo {
    pub name: String,
    pub done: bool,
    pub priority: Priority,
    pub created_at: DateTime<Local>,
    pub last_updated: DateTime<Local>,
}

impl Todo {
    pub fn new(name: String) -> Self {
        Self {
            name,
            done: false,
            priority: Priority::Normal,
            created_at: Local::now(),
            last_updated: Local::now(),
        }
    }

    pub fn toggle(&mut self) {
        self.done = !self.done;
        self.last_updated = Local::now();
    }
}

#[derive(ValueEnum, Serialize, Deserialize, Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub enum Priority {
    Lowest,
    Low,
    Normal,
    High,
    Highest,
}

impl std::fmt::Display for Priority {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Priority::Highest => "Highest".red(),
            Priority::High => "High".yellow(),
            Priority::Normal => "Normal".green(),
            Priority::Low => "Low".cyan(),
            Priority::Lowest => "Lowest".blue(),
        };
        write!(f, "{}", s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_todo() {
        let todo = Todo::new("Test".to_string());
        assert_eq!(todo.name, "Test");
        assert!(!todo.done);
    }

    #[test]
    fn test_toggle_todo() {
        let mut todo = Todo::new("Test".to_string());
        todo.toggle();
        assert!(todo.done);
        todo.toggle();
        assert!(!todo.done);
    }

    #[test]
    fn test_priority() {
        let mut todo = Todo::new("Test".to_string());

        assert_eq!(todo.priority, Priority::Normal);
        todo.priority = Priority::High;
        assert_eq!(todo.priority, Priority::High);
    }
}
