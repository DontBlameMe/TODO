use std::{
    fs::File,
    io::{Read, Write},
};

use crate::todo::Todo;
pub fn load_disk_data(file_name: &str) -> Vec<Todo> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("todo").unwrap();
    let data_path = xdg_dirs.place_data_file(file_name).unwrap();

    if !data_path.exists() {
        File::create(&data_path).unwrap();
    }

    let mut data_file = File::open(&data_path).unwrap();
    let mut data = String::new();
    data_file.read_to_string(&mut data).unwrap();

    serde_json::from_str(&data).unwrap_or_else(|_| Vec::new()) // Return empty Vec on parse error
}

pub fn save_disk_data(data: &Vec<Todo>, file_name: &str) {
    let json_data = serde_json::to_string(data).unwrap();
    let xdg_dirs = xdg::BaseDirectories::with_prefix("todo").unwrap();
    let data_path = xdg_dirs.place_data_file(file_name).unwrap();

    let mut data_file = File::create(&data_path).unwrap();
    data_file.write_all(json_data.as_bytes()).unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::todo::{Priority, Todo};
    use chrono::Local;

    #[test]
    fn test_load_disk_data() {
        let file_name = "test_load_disk_data.json";
        let data = load_disk_data(file_name);
        assert_eq!(data.len(), 0);

        delete_file(file_name);
    }

    #[test]
    fn test_save_disk_data() {
        let data = vec![Todo {
            name: "Test".to_string(),
            done: false,
            priority: Priority::Normal,
            created_at: Local::now(),
            last_updated: Local::now(),
        }];
        let file_name = "test_save_disk_data.json";

        save_disk_data(&data, file_name);

        let loaded_data = load_disk_data(file_name);

        assert_eq!(loaded_data.len(), 1);
        assert_eq!(loaded_data[0].name, "Test");

        delete_file(file_name);
    }

    fn delete_file(file_name: &str) {
        let xdg_dirs = xdg::BaseDirectories::with_prefix("todo").unwrap();
        let data_path = xdg_dirs.place_data_file(file_name).unwrap();

        if data_path.exists() {
            std::fs::remove_file(data_path).unwrap();
        }
    }
}
