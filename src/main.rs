use clap::Parser;
use cli::{Action, Cli};
use colored::Colorize;
use todo::Todo;

mod cli;
mod disk_data;
mod todo;

const DATA_FILE_NAME: &str = "data.json";

fn main() {
    let mut data = disk_data::load_disk_data(DATA_FILE_NAME);
    let cli = Cli::parse();

    match &cli.action.unwrap_or(Action::Status) {
        Action::Add { todo_name } => {
            handle_add(&mut data, &get_todo_name(todo_name));
        }
        Action::Remove { todo_name } => {
            handle_remove(&mut data, &get_todo_name(todo_name));
        }
        Action::Status => {
            handle_status(&mut data);
        }
        Action::Toggle { todo_name } => {
            handle_toggle(&mut data, &get_todo_name(todo_name));
        }
        Action::Clean => {
            handle_clean(&mut data);
        }
        Action::Priority {
            todo_name,
            priority,
        } => {
            handle_priority(&mut data, &get_todo_name(todo_name), priority);
        }
    }

    disk_data::save_disk_data(&data, DATA_FILE_NAME);
}

fn get_todo_name(name: &str) -> String {
    name.to_lowercase().replace(" ", "_").trim().to_string()
}

fn handle_priority(data: &mut [Todo], todo_name: &String, priority: &todo::Priority) {
    if todo_name.is_empty() {
        println!("{}", "Please provide a name for the todo".red());
        return;
    }

    if let Some(index) = data.iter().position(|todo| todo.name == *todo_name) {
        data[index].priority = priority.clone();
        println!(
            "{}",
            &format!(
                "Successfully set the priority of the todo {} to {}",
                todo_name.bold(),
                priority
            )
            .green()
        );
        show_status(data);
        return;
    }

    println!(
        "{}",
        &format!("There is no todo with the name {}", todo_name.bold()).red()
    );
}

fn handle_add(data: &mut Vec<Todo>, todo_name: &String) {
    if todo_name.is_empty() {
        println!("{}", "Please provide a name for the todo".red());
        return;
    }

    if data.iter().any(|todo| todo.name == *todo_name) {
        println!(
            "{}",
            &format!("A todo with the name {} already exists", todo_name.bold()).red(),
        );
        return;
    }

    let todo = todo::Todo::new(todo_name.to_string());

    println!(
        "{}",
        &format!("Successfully added the todo {}", &todo.name).green()
    );
    data.push(todo);
    show_status(data);
}

fn handle_remove(data: &mut Vec<Todo>, todo_name: &String) {
    if todo_name.is_empty() {
        println!("{}", "Please provide a name for the todo".red());
        return;
    }

    if let Some(index) = data.iter().position(|todo| todo.name == *todo_name) {
        data.remove(index);
        println!(
            "{}",
            &format!("Successfully removed the todo {}", todo_name.bold()).green()
        );
        show_status(data);
        return;
    }

    println!(
        "{}",
        &format!("There is no todo with the name {}", todo_name.bold()).red()
    );
}

fn handle_status(data: &mut [Todo]) {
    show_status(data);
}

fn handle_toggle(data: &mut [Todo], todo_name: &String) {
    if todo_name.is_empty() {
        println!("{}", "Please provide a name for the todo".red());
        return;
    }

    if let Some(index) = data.iter().position(|todo| todo.name == *todo_name) {
        data[index].toggle();
        show_status(data);
        return;
    }

    println!(
        "{}",
        &format!("There is no todo with the name {}", todo_name.bold()).red()
    );
}

fn handle_clean(data: &mut Vec<Todo>) {
    data.retain(|todo| !todo.done);
    show_status(data);
}

fn show_status(data: &[todo::Todo]) {
    if data.is_empty() {
        println!("{}", "No todos found.".white());
        return;
    }

    let mut sorted_data = data.to_vec();

    sorted_data.sort_by(|a, b| b.priority.cmp(&a.priority));
    sorted_data.sort_by(|a, b| a.done.cmp(&b.done));

    for todo in sorted_data {
        let created_at = &todo.created_at.format("%d.%m.%Y %H:%M").to_string().white();
        let last_updated = &todo
            .last_updated
            .format("%d.%m.%Y %H:%M")
            .to_string()
            .white();
        let done = if todo.done {
            "󰄲".green()
        } else {
            "󰱒".red()
        };

        println!(
            "{} {} {} {}{} {} {} {}{}",
            todo.priority,
            done,
            &todo.name.purple(),
            "(".black(),
            "󰙴".yellow(),
            created_at,
            "".cyan(),
            last_updated,
            ")".black(),
        );
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_handle_add() {
        let mut data = Vec::new();

        handle_add(&mut data, &"test".to_string());
        assert_eq!(data.len(), 1);
    }

    #[test]
    fn test_handle_invalid_add() {
        let mut data = Vec::new();

        handle_add(&mut data, &"".to_string());
        assert_eq!(data.len(), 0);
    }

    #[test]
    fn test_handle_duplicate_add() {
        let mut data = vec![Todo::new("test".to_string())];

        handle_add(&mut data, &"test".to_string());
        assert_eq!(data.len(), 1);
    }

    #[test]
    fn test_handle_remove() {
        let mut data = vec![Todo::new("test".to_string())];

        handle_remove(&mut data, &"test".to_string());
        assert_eq!(data.len(), 0);
    }

    #[test]
    fn test_handle_invalid_remove() {
        let mut data = vec![Todo::new("test".to_string())];

        handle_remove(&mut data, &"".to_string());
        assert_eq!(data.len(), 1);
    }

    #[test]
    fn test_handle_toggle() {
        let mut data = vec![Todo::new("test".to_string())];

        handle_toggle(&mut data, &"test".to_string());
        assert!(data[0].done);

        handle_toggle(&mut data, &"test".to_string());
        assert!(!data[0].done);

        handle_toggle(&mut data, &"test".to_string());
        assert!(data[0].done);
    }

    #[test]
    fn test_handle_purge() {
        let mut data = vec![Todo::new("test".to_string())];

        assert!(!data[0].done);
        handle_toggle(&mut data, &"test".to_string());
        assert!(data[0].done);
        handle_clean(&mut data);
        assert_eq!(data.len(), 0);
    }
}
